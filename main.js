function estimateProjectCompletionTime(teamProductivity, backlog, deadline) {
  const totalProductivity = teamProductivity.reduce((acc, curr) => acc + curr, 0);
  const totalBacklogPoints = backlog.reduce((acc, curr) => acc + curr, 0);
  const remainingDays = Math.ceil((deadline.getTime() - Date.now()) / (1000 * 60 * 60 * 24));
  const remainingWeekdays = getWeekdaysBetweenDates(new Date(), deadline);

  const requiredHours = (totalBacklogPoints / totalProductivity) / 8 / remainingWeekdays;
  const requiredHoursRounded = Math.round(requiredHours * 10) / 10;

  if (requiredHoursRounded <= 8) {
    const completionTime = Math.ceil(requiredHoursRounded * remainingWeekdays);
    console.log(`Усі завдання будуть успішно виконані за ${completionTime} днів до настання дедлайну!`);
  } else {
    const extraHours = Math.round((requiredHoursRounded - 8) * remainingWeekdays * 10) / 10;
    console.log(`Команді розробників доведеться витратити додатково ${extraHours} годин після дедлайну, щоб виконати всі завдання в беклозі`);
  }
}

function getWeekdaysBetweenDates(startDate, endDate) {
  let weekdays = 0;
  let currentDate = new Date(startDate);
  while (currentDate <= endDate) {
    const dayOfWeek = currentDate.getDay();
    if (dayOfWeek !== 0 && dayOfWeek !== 6) {
      weekdays++;
    }
    currentDate.setDate(currentDate.getDate() + 1);
  }
  return weekdays;
}